// This class measures exercised combinations of DUTs interface ports.
class timer_t_coverage extends uvm_subscriber #(timer_t_transaction);

    // registration of component tools
    `uvm_component_utils( timer_t_coverage )

    // member attributes
    local T m_transaction_h;
    virtual dut_internal_if ivif;

    // Covergroup definition
    covergroup FunctionalCoverage( string inst );
        /* INSERT YOUR CODE HERE */
        cp_mode : coverpoint ivif.ctrl_reg_d{
            bins DISABLED = {2'b00};
            bins AUTO_RESTART = {2'b01};
            bins ONE_SHOT = {2'b10};
            bins CONTINOUS = {2'b11};
            option.weight = 0;
        }

        cp_request: coverpoint m_transaction_h.REQUEST {
            bins NONE = { 2'b00 };
            bins READ = { 2'b01 };
            bins WRITE = { 2'b10 };
            ignore_bins RESERVED = { 2'b11 };
            option.weight = 0;
        }

        cp_reset: coverpoint m_transaction_h.RST {
            bins ACTIVE = { 0 };
            bins NOT_ACTIVE = { 1 };
        }

        cp_reset_transition: coverpoint m_transaction_h.RST {
            bins ACTIVE_TO_NOT_ACTIVE = (0 => 1);
            bins NOT_ACTIVE_TO_ACTIVE = (1 => 0);
            option.at_least = 5;
        }

        cp_address: coverpoint m_transaction_h.ADDRESS {
            bins CNT = { 8'h00 };
            bins CMP = { 8'h04 };
            bins CR = { 8'h08 };
            bins CYCLE_L = { 8'h10 };
            bins CYCLE_H = { 8'h14 };
            option.weight = 0;
        }

        cr_address_write: cross cp_address, cp_request, cp_reset {
            ignore_bins write_active = ! binsof(cp_reset.NOT_ACTIVE) || ! binsof(cp_request.WRITE);
            ignore_bins read_only = binsof(cp_address.CYCLE_L) || binsof(cp_address.CYCLE_H);
        }

        cr_address_read: cross cp_address, cp_request, cp_reset {
            ignore_bins read_active = ! binsof(cp_reset.NOT_ACTIVE) || ! binsof(cp_request.READ);
        }

        cp_irq: coverpoint m_transaction_h.P_IRQ {
            bins ACTIVE = { 1 };
            bins NOT_ACTIVE = { 0 };
        }

        cp_irq_transition: coverpoint m_transaction_h.P_IRQ {
            bins ACTIVE_TO_NOT_ACTIVE = (1 => 0);
            bins NOT_ACTIVE_TO_ACTIVE = (0 => 1);
            option.at_least = 10;
        }

        cr_irq_mode: cross cp_irq, cp_mode {
            ignore_bins disabled = ! binsof(cp_irq.ACTIVE) || binsof(cp_mode.DISABLED);
        }

        cp_mode_transition: coverpoint ivif.ctrl_reg_d {
            bins transitions[] = ( [0:3] => [0:3] );
        }

        cr_addreess_request: cross cp_address, cp_request, cp_reset {
            ignore_bins reset = binsof(cp_reset.ACTIVE);
            ignore_bins write_cycle_h = binsof(cp_request.WRITE) && binsof(cp_address.CYCLE_H);
            ignore_bins write_cycle_l = binsof(cp_request.WRITE) && binsof(cp_address.CYCLE_L);
        }

    endgroup

    // Constructor - creates new instance of this class
    function new( string name = "m_coverage_h", uvm_component parent = null );
        super.new( name, parent );
        FunctionalCoverage = new( "timer" );
    endfunction: new

    // Build - instantiates child components
    function void build_phase( uvm_phase phase );
        super.build_phase( phase );
        if ( !uvm_config_db #(virtual dut_internal_if)::get(this,
            "*", "dut_internal_if", ivif) ) begin
            `uvm_fatal( "configuration:", "Cannot find 'dut_internal_if' inside uvm_config_db, probably not set!" )
        end
    endfunction: build_phase

    // Write - obligatory function, samples value on the interface.
    function void write( T t );
        // skip invalid transactions
        m_transaction_h = t;
        FunctionalCoverage.sample();
    endfunction: write

endclass: timer_t_coverage

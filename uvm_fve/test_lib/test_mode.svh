class timer_t_test_mode extends timer_t_test_base;

    // registration of component tools
    `uvm_component_utils( timer_t_test_mode )

    uvm_sequence_base seq;
    // Constructor - creates new instance of this class
    function new( string name = "timer_t_test_mode", uvm_component parent = null );
        super.new( name, parent );
    endfunction: new

    // Build - instantiates child components
    function void build_phase( uvm_phase phase );
        super.build_phase( phase );
    endfunction: build_phase

    // Run - start processing sequences
    task run_phase( uvm_phase phase );
        // creation of sequences
        uvm_sequence_base rst_seq = timer_t_sequence_reset::type_id::create( "reset" );
        uvm_sequence_base mode_disabled_seq = timer_t_sequence_mode_disabled::type_id::create( "mode-disabled" );
        uvm_sequence_base mode_auto_restart_seq = timer_t_sequence_mode_auto_restart::type_id::create( "mode-auto-restart" );
        uvm_sequence_base mode_one_shot_seq = timer_t_sequence_mode_one_shot::type_id::create( "mode-one-shot" );
        uvm_sequence_base mode_continuous_seq = timer_t_sequence_mode_continuous::type_id::create( "mode-continuous" );
        uvm_sequence_base mode_transitions_seq = timer_t_sequence_mode_transitions::type_id::create( "mode-transitions" );

        // prevent the phase from immediate termination
        phase.raise_objection( this );

        // starting reset sequence
        rst_seq.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );
        mode_disabled_seq.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );
        mode_auto_restart_seq.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );
        mode_one_shot_seq.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );
        mode_continuous_seq.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );
        mode_transitions_seq.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );

        phase.drop_objection( this );

    endtask: run_phase

endclass: timer_t_test_mode

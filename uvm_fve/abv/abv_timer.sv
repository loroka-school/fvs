`include "uvm_macros.svh"
import uvm_pkg::*;
import sv_param_pkg::*;

module abv_timer (
    input logic                  CLK,
    input logic                  RST,
    input logic                  P_IRQ,
    input logic [ADDR_WIDTH-1:0] ADDRESS,
    input logic [1:0]            REQUEST,
    input logic [2:0]            RESPONSE,
    input logic [DATA_WIDTH-1:0] DATA_OUT,
    input logic [DATA_WIDTH-1:0] DATA_IN,
    input logic [1:0]            ctrl_reg_d,
    input logic [DATA_WIDTH-1:0] cnt_reg_d,
    input logic [DATA_WIDTH-1:0] cmp_reg_d,
    input logic [63:0]           cycle_cnt
);

function int rw();
    if ((REQUEST == CP_REQ_READ) || (REQUEST == CP_REQ_WRITE))
        return 1;
    else
        return 0;
endfunction: rw


// helper function
function int valid_address();
    if (ADDRESS > 8'h14 || (ADDRESS & 8'h03) > 0)
        return 0;
    else
        return 1;
endfunction: valid_address

/** Checking unknown values of DATA_IN during write request. */
property pr_write;
    @(posedge CLK) (REQUEST == CP_REQ_WRITE) |-> !$isunknown(DATA_IN);
endproperty
a_write: assert property (pr_write);
c_write: cover property (pr_write);


// [assert] Řídící signály (ADDRESS, REQUEST, RESPONSE a P_IRQ) nesmí mít nedefinované hodnoty (X,Z)
property pr_def;
    /* check reset to ignore the starting undefined signals */
    @(posedge CLK) !$isunknown(RST) |-> !$isunknown(ADDRESS) && !$isunknown(REQUEST) && !$isunknown(RESPONSE) && !$isunknown(P_IRQ);
endproperty
a_def: assert property (pr_def);


// Čtené (DATA_OUT) a zapisované (DATA_IN) data nesmí mít nedefinované hodnoty (X,Z). Vyhodnocení jen při operacích READ a WRITE
property pr_rw_def;
    @(posedge CLK)  rw() |-> !$isunknown(DATA_IN) && !$isunknown(DATA_OUT);
endproperty
a_rw_def: assert property (pr_rw_def);


// [assert+ cover] Při zápisu/čtení na/z adresy mimo adresový prostor timeru je v dalším cyklu nastaven RESPONSE OOR
property pr_rw_oor;
    @(posedge CLK) disable iff (!RST)
                   rw() && (ADDRESS > 8'h14) |=> (RESPONSE == CP_RSP_OOR);
endproperty
a_rw_oor: assert property (pr_rw_oor);
c_rw_oor: cover property (pr_rw_oor);


// TODO check why has false positves
// [assert+ cover] Při zápisu/čtení na/z nezarovnané adresy v adresovém prostoru timeru je v dalším cyklu nastaven RESPONSE UNALIGNED
property pr_rw_unaligned;
    @(posedge CLK) disable iff (!RST || (ADDRESS[0] == 0 && ADDRESS[1] == 0) || ADDRESS > 8'h14)
                   ((REQUEST == CP_REQ_READ) || (REQUEST == CP_REQ_WRITE)) |=> (RESPONSE == CP_RSP_UNALIGNED);
endproperty
a_rw_unaligned: assert property (pr_rw_unaligned);
c_rw_unaligned: cover property (pr_rw_unaligned);


// [assert+ cover] Kontrola zápisu a čtení v po sobě jdoucích cyklech na stejnou adresu. Pročtená data nejsou ovlivněna zápisem.
property pr_rw_transition_cnt;
// (ADDRESS == $past(ADDRESS))
    @(posedge CLK) disable iff (!RST)
                   (REQUEST == CP_REQ_READ) && ($past(REQUEST) == CP_REQ_WRITE) && (ADDRESS == $past(ADDRESS)) && (ADDRESS == TIMER_CNT) |=> $past(cnt_reg_d) == DATA_OUT;
endproperty
a_rw_transition_cnt: assert property (pr_rw_transition_cnt);
c_rw_transition_cnt: cover property (pr_rw_transition_cnt);

property pr_rw_transition_cmp;
// (ADDRESS == $past(ADDRESS))
    @(posedge CLK) disable iff (!RST)
                   (REQUEST == CP_REQ_READ) && ($past(REQUEST) == CP_REQ_WRITE) && (ADDRESS == $past(ADDRESS)) && (ADDRESS == TIMER_CMP) |=> $past(cmp_reg_d) == DATA_OUT;
endproperty
a_rw_transition_cmp: assert property (pr_rw_transition_cmp);
c_rw_transition_cmp: cover property (pr_rw_transition_cmp);

property pr_rw_transition_ctrl;
// (ADDRESS == $past(ADDRESS))
    @(posedge CLK) disable iff (!RST)
                   (REQUEST == CP_REQ_READ) && ($past(REQUEST) == CP_REQ_WRITE) && (ADDRESS == $past(ADDRESS)) && (ADDRESS == TIMER_CR) |=> $past(ctrl_reg_d) == DATA_OUT;
endproperty
a_rw_transition_ctrl: assert property (pr_rw_transition_ctrl);
c_rw_transition_ctrl: cover property (pr_rw_transition_ctrl);

// TODO 
// [assert+ cover] Při zápisu/čtení na/z správnou adresu v adresovém prostoru timeru je v dalším cyklu nastaven RESPONSE ACK.
property pr_rw_valid;
    @(posedge CLK) disable iff (!RST) 
                   rw() && valid_address() |=> (RESPONSE == CP_RSP_ACK);
endproperty
a_rw_valid: assert property (pr_rw_valid);
c_rw_valid: cover property (pr_rw_valid);


// [assert+ cover] Odpověď na NONE REQUEST je vždy IDLE
property pr_req_none;
    @(posedge CLK) (REQUEST == CP_REQ_NONE) |=> (RESPONSE == CP_RSP_IDLE);
endproperty
a_req_none: assert property (pr_req_none);
c_req_none: cover property (pr_req_none);


// TODO check it triggers
// [assert+ cover] Odpověď na RESERVED REQUEST je vždy ERROR
property pr_req_reserved;
    @(posedge CLK) disable iff (!RST)
                   (REQUEST == CP_REQ_RESERVED) |=> (RESPONSE == CP_RSP_ERROR);
endproperty
a_req_reserved: assert property (pr_req_reserved);
c_req_reserved: cover property (pr_req_reserved);


// [assert] Odpověď WAIT se nesmí nikdy objevit.
property pr_resp_wait;
    @(posedge CLK) (RESPONSE == CP_RSP_WAIT) |-> 0;
endproperty
a_resp_wait: assert property (pr_resp_wait);


// [assert+ cover] Když se hodnota v cnt_reg rovná hodnotě v cmp_reg, v dalším cyklu je P_IRQ nastavenna 1 (neplatí v módu DISABLED), 
// jinak má hodnotu0. 
property pr_active_cmp;
    @(posedge CLK) disable iff (!RST)
                   (cnt_reg_d == cmp_reg_d) && (ctrl_reg_d != MODE_DISABLED) |=> (P_IRQ == 1'b1);
endproperty
a_active_cmp: assert property (pr_active_cmp);
c_active_cmp: cover property (pr_active_cmp);

property pr_inactive_cmp;
    @(posedge CLK) (ctrl_reg_d == MODE_DISABLED) |=> (P_IRQ == 1'b0);
endproperty
a_inactive_cmp: assert property (pr_inactive_cmp);
c_inactive_cmp: cover property (pr_inactive_cmp);


// [assert+ cover] Když se hodnota v cnt_reg rovná hodnotě v cmp_reg a mód je nastaven na AUTO_RESTART, v dalším cyklu je cnt_reg vynulován
property pr_mode_autorestart;
    // check it is not written into register on count reset
    @(posedge CLK) disable iff ((REQUEST == CP_REQ_WRITE) && (ADDRESS == TIMER_CNT))
                   (cnt_reg_d == cmp_reg_d) && (ctrl_reg_d == MODE_AUTO_RESTART) |=> (cnt_reg_d == 0);
endproperty
a_mode_autorestart: assert property (pr_mode_autorestart);
c_mode_autorestart: cover property (pr_mode_autorestart);


// [assert+ cover] Když se hodnota v cnt_reg rovná hodnotě v cmp_reg a mód je nastaven na CONTINUOUS, 
// v dalším cyklu je cnt_reg hodnota inkrementována o jedna oproti minulému cyklu.
property pr_mode_continuous;
    @(posedge CLK) disable iff (((REQUEST == CP_REQ_WRITE) && (ADDRESS == TIMER_CNT)) || !RST) 
                   (cnt_reg_d == cmp_reg_d) && (ctrl_reg_d == MODE_CONTINOUS) |=> (cnt_reg_d == $past(cnt_reg_d) + 1);
endproperty
a_mode_continuous: assert property (pr_mode_continuous);
c_mode_continuous: cover property (pr_mode_continuous);


// [assert+ cover] Když se hodnota v cnt_reg rovná hodnotě v cmp_reg a mód je nastaven na ONE_SHOT, 
// v dalším cyklu je cnt_reg vynulován a mód nastaven na DISABLED
property pr_mode_one_shot;
    @(posedge CLK) disable iff ((REQUEST == CP_REQ_WRITE) && (ADDRESS == TIMER_CR))
                   (cnt_reg_d == cmp_reg_d) && (ctrl_reg_d == MODE_ONE_SHOT) |=> (ctrl_reg_d == MODE_DISABLED);
endproperty
a_mode_one_shot: assert property (pr_mode_one_shot);
c_mode_one_shot: cover property (pr_mode_one_shot);


// [assert+ cover] Čtení z CYCLE_L vrací v dalším cyklu spodní slovo čítače cycle_cnt
property pr_read_cycle_l;
    @(posedge CLK) disable iff (!RST)
                   (REQUEST == CP_REQ_READ) && (ADDRESS == TIMER_CYCLE_L) |=> (DATA_OUT == (cycle_cnt & 32'hffffffff));
endproperty
a_read_cycle_l: assert property (pr_read_cycle_l);
c_read_cycle_l: cover property (pr_read_cycle_l);

// [assert+ cover] Čtení z CYCLE_H vrací vdalším cyklu horní slovo čítače cycle_cnt
property pr_read_cycle_h;
    @(posedge CLK) disable iff (!RST)
                   (REQUEST == CP_REQ_READ) && (ADDRESS == TIMER_CYCLE_H) |=> (DATA_OUT == ((cycle_cnt & (32'hffffffff << 32)) >> 32));
endproperty
a_read_cycle_h: assert property (pr_read_cycle_h);
c_read_cycle_h: cover property (pr_read_cycle_h);


// TODO how to fix HDL code?
// [assert+ cover] cycle_cnt má v průběhu aktivního resetu hodnotu 0
property pr_cycle_reset;
    @(posedge CLK) (RST == RST_ACT_LEVEL) |-> (cycle_cnt == 0);
endproperty
a_cycle_reset: assert property (pr_cycle_reset);
c_cycle_reset: cover property (pr_cycle_reset);


endmodule : abv_timer

// Represents the golden model of the processor used to predict results of the DUT.
class timer_t_gm extends uvm_subscriber #(timer_t_transaction);//uvm_component;

    // registration of component tools
    `uvm_component_utils( timer_t_gm )

    // analysis port for outside components to access transactions from the monitor
    uvm_analysis_port #(timer_t_transaction) timer_t_analysis_port;

    // static local variables accesible by waveform
    static logic                  P_IRQ;
    static logic [2:0]            RESPONSE;
    static logic [DATA_WIDTH-1:0] DATA_OUT;

    static logic [DATA_WIDTH-1:0] COMPARE = 0;
    static logic [DATA_WIDTH-1:0] COMPARE_Q = 0;

    static logic [DATA_WIDTH-1:0] COUNTER = 0;
    static logic [DATA_WIDTH-1:0] COUNTER_Q = 0;

    static logic [63:0] CYCLE = -1;

    // local variables
    local logic [ADDR_WIDTH-1:0] ADDRESS;
    local logic [DATA_WIDTH-1:0] DATA_IN;
    local logic [2:0] REQUEST;
    local logic RST;

    local logic[DATA_WIDTH-1:0]  MODE = MODE_DISABLED;
    local logic[DATA_WIDTH-1:0]  MODE_Q = MODE_DISABLED;

    // base name prefix for created transactions
    string m_name = "gold";

    // Constructor - creates new instance of this class
    function new( string name = "m_timer_t_gm_h", uvm_component parent = null );
        super.new( name, parent );
    endfunction: new

    // Build - instantiates child components
    function void build_phase( uvm_phase phase );
    	super.build_phase( phase );

      timer_t_analysis_port = new( "timer_t_analysis_port", this );

    endfunction: build_phase

    // Connect - create interconnection between child components
    function void connect_phase( uvm_phase phase );
        super.connect_phase( phase );
    endfunction: connect_phase

    // Write - get all transactions from driver for computing predictions
    function void write( T t );
  		timer_t_transaction out_t;


      out_t = timer_t_transaction::type_id::create(
          $sformatf("%0s: %0t", m_name, $time) );

      out_t.copy(t);

      // predict outputs
      predict( out_t );

      // support function for displaying data in wave
      wave_display_support_func(out_t);

      // send predicted outputs to scoreboard
      timer_t_analysis_port.write(out_t);
  	endfunction: write

    // implements behavior of the golden model
    local function automatic void predict( timer_t_transaction t );
        if (t.RST == RST_ACT_LEVEL)
            set_default_outputs(t);
        else
            begin
                t.DATA_OUT = 0;

                inc_counter(t);
                cmp_counter(t);

                case (REQUEST)
                    CP_REQ_NONE: t.RESPONSE = CP_RSP_IDLE;
                    CP_REQ_READ: sim_read(t);
                    CP_REQ_WRITE: sim_write(t);
                    CP_REQ_RESERVED: t.RESPONSE = CP_RSP_ERROR;
                endcase
            end;

        ADDRESS = t.ADDRESS;
        REQUEST = t.REQUEST;
        DATA_IN = t.DATA_IN;
    endfunction: predict

    local function void inc_counter( timer_t_transaction t );
        /* remmember previous value for "register effect" */
        COUNTER_Q = COUNTER;

        if (MODE != MODE_DISABLED)
            COUNTER = COUNTER + 1;
        CYCLE = CYCLE + 1;
    endfunction: inc_counter

    local function void cmp_counter( timer_t_transaction t );
        /* remmember previous value for "register effect" */   
        MODE_Q = MODE;
        COMPARE_Q = COMPARE;

        t.P_IRQ = 0;

        if (COUNTER == COMPARE + 1) begin
            case (MODE)
                MODE_AUTO_RESTART: begin
                    t.P_IRQ = 1;
                    COUNTER = 0;
                end
                MODE_ONE_SHOT: begin
                    t.P_IRQ = 1;
                    COUNTER = 0;
                    MODE = MODE_DISABLED;
                end
                MODE_CONTINOUS:
                    t.P_IRQ = 1;
            endcase
        end;
    endfunction: cmp_counter

    local function int check_address( timer_t_transaction t);
        if (ADDRESS > 8'h14)
            t.RESPONSE = CP_RSP_OOR;
        else if (ADDRESS & 8'h03 > 0)
            t.RESPONSE = CP_RSP_UNALIGNED;
        else
            t.RESPONSE = CP_RSP_ACK;
            return 0;

        return -1;
    endfunction: check_address

    local function void sim_read( timer_t_transaction t);
        if (check_address(t) == 0) begin
            case (ADDRESS)
                TIMER_CNT: t.DATA_OUT = COUNTER_Q;
                TIMER_CMP: t.DATA_OUT = COMPARE_Q;
                TIMER_CR: t.DATA_OUT = MODE_Q;
                TIMER_CYCLE_L: t.DATA_OUT = CYCLE & 32'hffffffff;
                TIMER_CYCLE_H: t.DATA_OUT = (CYCLE & (32'hffffffff << 32)) >> 32;
            endcase
        end;
    endfunction: sim_read

    local function void sim_write( timer_t_transaction t);
        if (check_address(t) == 0) begin
            case (ADDRESS)
                TIMER_CNT: COUNTER = DATA_IN;
                TIMER_CMP: COMPARE = DATA_IN;
                TIMER_CR: MODE = DATA_IN & 8'h03;
            endcase
        end;
    endfunction: sim_write

    local function void set_default_outputs( timer_t_transaction t );
        t.P_IRQ    = 0;
        t.RESPONSE = 0;
        t.DATA_OUT = 0;
        t.REQUEST = 0;
        REQUEST = 0;

        CYCLE = -1;
        COMPARE = 0;
        MODE = MODE_DISABLED;
        COUNTER_Q = 0;
        MODE_Q = MODE_DISABLED;
        COUNTER = 0;

    endfunction: set_default_outputs

    local function automatic void wave_display_support_func( timer_t_transaction t );
        P_IRQ    = t.P_IRQ;
        RESPONSE = t.RESPONSE;
        DATA_OUT = t.DATA_OUT;
    endfunction: wave_display_support_func

endclass: timer_t_gm

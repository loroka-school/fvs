class timer_t_sequence_mode_disabled extends timer_t_sequence;

    // registration of object tools
    `uvm_object_utils( timer_t_sequence_mode_disabled )

    // Constructor - creates new instance of this class
    function new( string name = "timer_t_sequence_mode_disabled" );
        super.new( name );
    endfunction: new

    // body - implements behavior of the reset sequence (unidirectional)
    task body();
        // set reset values, randomize() cannot be used here
        default_RST     = ~RST_ACT_LEVEL;
        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;

        for (int i=0; i<5; i++) begin
            create_and_finish_item();
        end;

    endtask: body

endclass: timer_t_sequence_mode_disabled


class timer_t_sequence_mode_auto_restart extends timer_t_sequence;

    // registration of object tools
    `uvm_object_utils( timer_t_sequence_mode_auto_restart )

    // Constructor - creates new instance of this class
    function new( string name = "timer_t_sequence_mode_auto_restart" );
        super.new( name );
    endfunction: new

    // body - implements behavior of the reset sequence (unidirectional)
    task body();
        // set reset values, randomize() cannot be used here
        default_RST     = ~RST_ACT_LEVEL;
        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 3;
        create_and_finish_item();

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_AUTO_RESTART;
        create_and_finish_item();

        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;

        for (int i=0; i<10; i++) begin
            create_and_finish_item();
        end;
         // test when CNT == CMP
        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 3;
        create_and_finish_item();

        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();
        create_and_finish_item();

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 0;
        create_and_finish_item();

        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 32'hfffffffe;
        create_and_finish_item();

        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();
        create_and_finish_item();
        create_and_finish_item();
        create_and_finish_item();
        create_and_finish_item();
        create_and_finish_item();
        create_and_finish_item();





    endtask: body

endclass: timer_t_sequence_mode_auto_restart


class timer_t_sequence_mode_one_shot extends timer_t_sequence;

    // registration of object tools
    `uvm_object_utils( timer_t_sequence_mode_one_shot )

    // Constructor - creates new instance of this class
    function new( string name = "timer_t_sequence_mode_one_shot" );
        super.new( name );
    endfunction: new

    // body - implements behavior of the reset sequence (unidirectional)
    task body();
        // set reset values, randomize() cannot be used here
        default_RST     = ~RST_ACT_LEVEL;
        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 6;
        create_and_finish_item();

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_ONE_SHOT;
        create_and_finish_item();

        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;

        for (int i=0; i<10; i++) begin
            create_and_finish_item();
        end;

        // test when CNT == CMP
        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 6;
        create_and_finish_item();

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_ONE_SHOT;
        create_and_finish_item();

        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();
        create_and_finish_item();


        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 0;
        create_and_finish_item();

        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 32'hfffffffe;
        create_and_finish_item();

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_ONE_SHOT;
        create_and_finish_item();

        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();
        create_and_finish_item();
        create_and_finish_item();

    endtask: body

endclass: timer_t_sequence_mode_one_shot


class timer_t_sequence_mode_continuous extends timer_t_sequence;

    // registration of object tools
    `uvm_object_utils( timer_t_sequence_mode_continuous )

    // Constructor - creates new instance of this class
    function new( string name = "timer_t_sequence_mode_continuous" );
        super.new( name );
    endfunction: new

    // body - implements behavior of the reset sequence (unidirectional)
    task body();
        // set reset values, randomize() cannot be used here
        default_RST     = ~RST_ACT_LEVEL;
        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 3;
        create_and_finish_item();

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_CONTINOUS;
        create_and_finish_item();

        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;

        for (int i=0; i<5; i++) begin
            create_and_finish_item();
        end;

        // test when CNT == CMP
        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 3;
        create_and_finish_item();

        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();
        create_and_finish_item();

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 0;
        create_and_finish_item();


        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 32'hfffffffe;
        create_and_finish_item();

        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();
        create_and_finish_item();
        create_and_finish_item();

    endtask: body

endclass: timer_t_sequence_mode_continuous

class timer_t_sequence_mode_transitions extends timer_t_sequence;

    // registration of object tools
    `uvm_object_utils( timer_t_sequence_mode_transitions )

    // Constructor - creates new instance of this class
    function new( string name = "timer_t_sequence_mode_transitions" );
        super.new( name );
    endfunction: new

    // body - implements behavior of the reset sequence (unidirectional)
    task body();
        // set reset values, randomize() cannot be used here
        default_RST     = ~RST_ACT_LEVEL;
        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 3;
        create_and_finish_item();

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_CONTINOUS;
        create_and_finish_item();

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_ONE_SHOT;
        create_and_finish_item();

        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();
        create_and_finish_item();
        create_and_finish_item();
        
        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_CONTINOUS;
        create_and_finish_item();

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_AUTO_RESTART;
        create_and_finish_item();

        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_ONE_SHOT;
        create_and_finish_item();

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_CONTINOUS;
        create_and_finish_item();
        
        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_ONE_SHOT;
        create_and_finish_item();

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_AUTO_RESTART;
        create_and_finish_item();

    endtask: body

endclass: timer_t_sequence_mode_transitions

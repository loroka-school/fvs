# Signals of interfaces.
proc basic { PATH } {
    add wave -noupdate -divider "Basic signals"
    add wave -noupdate -color yellow -label CLK $PATH/CLK
    add wave -noupdate -color yellow -label RST $PATH/RST
}

proc itimer_itf { PATH } {
    set PROBE_PATH $PATH

    add wave -noupdate -divider "input"
    add wave -noupdate -hex -label ADDRESS $PROBE_PATH/ADDRESS
    add wave -noupdate -hex -label REQUEST $PROBE_PATH/REQUEST
    add wave -noupdate -hex -label DATA_IN $PROBE_PATH/DATA_IN

    add wave -noupdate -divider "TIMER DUT"
    add wave -noupdate -divider "output"
    add wave -noupdate -hex -label P_IRQ $PROBE_PATH/P_IRQ
    add wave -noupdate -hex -label RESPONSE $PROBE_PATH/RESPONSE
    add wave -noupdate -hex -label DATA_OUT $PROBE_PATH/DATA_OUT

    add wave -noupdate -divider "regs:"
    add wave -noupdate -decimal -label cnt_reg_d sim:/top/HDL_DUT_U/cnt_reg_d
    add wave -noupdate -hex -label cmp_reg_d sim:/top/HDL_DUT_U/cmp_reg_d
    add wave -noupdate -hex -label ctrl_reg_d sim:/top/HDL_DUT_U/ctrl_reg_d
    add wave -noupdate -decimal -label cycle_cnt sim:/top/HDL_DUT_U/cycle_cnt

    add wave -noupdate -divider "TIMER GM:"
    add wave -noupdate -divider "output"
    add wave -noupdate -hex -color orange -label P_IRQ sv_timer_t_gm_pkg::timer_t_gm::P_IRQ
    add wave -noupdate -hex -color orange -label RESPONSE sv_timer_t_gm_pkg::timer_t_gm::RESPONSE
    add wave -noupdate -hex -color orange -label DATA_OUT sv_timer_t_gm_pkg::timer_t_gm::DATA_OUT
    add wave -noupdate -decimal -color orange -label COUNTER sv_timer_t_gm_pkg::timer_t_gm::COUNTER
    add wave -noupdate -decimal -color orange -label CYCLE sv_timer_t_gm_pkg::timer_t_gm::CYCLE
    add wave -noupdate -decimal -color orange -label COMPARE sv_timer_t_gm_pkg::timer_t_gm::COMPARE

    add wave -noupdate -divider "assertions"
    add wave -noupdate -label pr_write /top/HDL_DUT_U/abv_timer_module/a_write
    add wave -noupdate -label pr_def /top/HDL_DUT_U/abv_timer_module/a_def
    add wave -noupdate -label pr_rw_def /top/HDL_DUT_U/abv_timer_module/a_rw_def
    add wave -noupdate -label pr_rw_oor /top/HDL_DUT_U/abv_timer_module/a_rw_oor
    add wave -noupdate -label pr_rw_unaligned /top/HDL_DUT_U/abv_timer_module/a_rw_unaligned
    add wave -noupdate -label pr_rw_transition_cnt /top/HDL_DUT_U/abv_timer_module/a_rw_transition_cnt
    add wave -noupdate -label pr_rw_transition_cmp /top/HDL_DUT_U/abv_timer_module/a_rw_transition_cmp
    add wave -noupdate -label pr_rw_transition_ctrl /top/HDL_DUT_U/abv_timer_module/a_rw_transition_ctrl
    add wave -noupdate -label pr_rw_valid /top/HDL_DUT_U/abv_timer_module/a_rw_valid
    add wave -noupdate -label pr_req_none /top/HDL_DUT_U/abv_timer_module/a_req_none
    add wave -noupdate -label pr_req_reserved /top/HDL_DUT_U/abv_timer_module/a_req_reserved
    add wave -noupdate -label pr_resp_wait /top/HDL_DUT_U/abv_timer_module/a_resp_wait
    add wave -noupdate -label pr_active_cmp /top/HDL_DUT_U/abv_timer_module/a_active_cmp
    add wave -noupdate -label pr_inactive_cmp /top/HDL_DUT_U/abv_timer_module/a_inactive_cmp
    add wave -noupdate -label pr_mode_autorestart /top/HDL_DUT_U/abv_timer_module/a_mode_autorestart
    add wave -noupdate -label pr_mode_continuous /top/HDL_DUT_U/abv_timer_module/a_mode_continuous
    add wave -noupdate -label pr_mode_one_shot /top/HDL_DUT_U/abv_timer_module/a_mode_one_shot
    add wave -noupdate -label pr_read_cycle_l /top/HDL_DUT_U/abv_timer_module/a_read_cycle_l
    add wave -noupdate -label pr_read_cycle_h /top/HDL_DUT_U/abv_timer_module/a_read_cycle_h
    add wave -noupdate -label pr_cycle_reset /top/HDL_DUT_U/abv_timer_module/a_cycle_reset

}

proc customize_gui { TOP_MODULE HDL_DUT } {
    # View simulation waves
    view wave
    # Remove all previous waves first
    delete wave *
    # Clock and reset signals
    puts $TOP_MODULE
    puts $HDL_DUT
    basic /top/timer_t_if
    itimer_itf top/timer_t_if

    # Additional wave configuration
    TreeUpdate [SetDefaultTree]
    configure wave -namecolwidth 200
    configure wave -valuecolwidth 100
    configure wave -justifyvalue left
    configure wave -signalnamewidth 0
    configure wave -gridoffset 0
    configure wave -gridperiod {10 ns}
    configure wave -griddelta 50
    configure wave -timeline 0
    configure wave -timelineunits ns
    WaveRestoreZoom {0 ns} {250 ns}
    update
    view structure
    view signals
    wave refresh
}

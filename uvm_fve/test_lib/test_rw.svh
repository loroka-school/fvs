class timer_t_test_rw extends timer_t_test_base;

    // registration of component tools
    `uvm_component_utils( timer_t_test_rw )

    uvm_sequence_base seq;
    // Constructor - creates new instance of this class
    function new( string name = "timer_t_test_rw", uvm_component parent = null );
        super.new( name, parent );
    endfunction: new

    // Build - instantiates child components
    function void build_phase( uvm_phase phase );
        super.build_phase( phase );
    endfunction: build_phase

    // Run - start processing sequences
    task run_phase( uvm_phase phase );
        // creation of sequences
        uvm_sequence_base rst_seq = timer_t_sequence_reset::type_id::create( "reset" );
        // read
        uvm_sequence_base read_valid_seq = timer_t_sequence_read_valid::type_id::create( "read_valid" );
        uvm_sequence_base read_unaligned_seq = timer_t_sequence_read_unaligned::type_id::create( "read_unaligned" );
        uvm_sequence_base read_oor_seq = timer_t_sequence_read_oor::type_id::create( "read_oor" );
        // write
        uvm_sequence_base write_valid_seq = timer_t_sequence_write_valid::type_id::create( "write_valid" );
        uvm_sequence_base write_unaligned_seq = timer_t_sequence_write_unaligned::type_id::create( "write_unaligned" );
        uvm_sequence_base write_oor_seq = timer_t_sequence_write_oor::type_id::create( "write_oor" );

        uvm_sequence_base rw_trans = timer_t_sequence_rw_trans::type_id::create( "rw_trans" );
        

        // prevent the phase from immediate termination
        phase.raise_objection( this );

        // starting reset sequence
        rst_seq.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );

        read_valid_seq.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );
        read_unaligned_seq.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );
        read_oor_seq.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );
        
        write_valid_seq.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );
        write_unaligned_seq.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );
        write_oor_seq.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );

        rw_trans.start( m_env_h.m_timer_t_agent_h.m_sequencer_h );

        phase.drop_objection( this );

    endtask: run_phase

endclass: timer_t_test_rw

class timer_t_sequence_random extends timer_t_sequence;
    /* INSERT YOUR CODE HERE */
    // registration of object tools
    `uvm_object_utils( timer_t_sequence_random )

	rand bit [3:0] selector;

	/* workaround for overlapping ranges */
	constraint dist_address {
		selector == 0 -> default_ADDRESS == TIMER_CNT;
		selector == 1 -> default_ADDRESS == TIMER_CMP;
		selector == 2 -> default_ADDRESS == TIMER_CR;
		selector == 3 -> default_ADDRESS inside {TIMER_CYCLE_L, TIMER_CYCLE_H};
		selector == 4 -> default_ADDRESS inside {[1:3], [5:7], [9:15], [17:19], [21:$]};
	}

    // default constraints for each input interface port
	constraint c_dist {
		default_RST dist {~RST_ACT_LEVEL:=20, RST_ACT_LEVEL:=1};
		selector dist {0:=7, 1:=6, 2:=5, 3:=2*2, 4:=1};
		default_REQUEST dist {CP_REQ_NONE:=10, CP_REQ_READ:=5, CP_REQ_WRITE:=5, CP_REQ_RESERVED:=1};
		default_DATA_IN dist {0:=10, [1:20]:=20*20, [21:$]:=1};
	}

    // Constructor - creates new instance of this class
	  function new( string name = "timer_t_sequence_random" );
		    super.new( name );
	  endfunction: new

  	// body - implements behavior of the reset sequence (unidirectional)
  	task body();
  	  // initialize PRNG
  	  this.srandom( SEED );
  	  repeat ( TRANSACTION_COUNT ) begin
  	    if ( !this.randomize() ) begin
  	      `uvm_error( "body:", "Failed to randomize!" )
  	    end
  	    create_and_finish_item();
  	  end
  	endtask: body
endclass: timer_t_sequence_random

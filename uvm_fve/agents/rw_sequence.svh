class timer_t_sequence_read_valid extends timer_t_sequence;

    // registration of object tools
    `uvm_object_utils( timer_t_sequence_read_valid )

    // Constructor - creates new instance of this class
    function new( string name = "timer_t_sequence_read_valid" );
        super.new( name );
    endfunction: new

    // body - implements behavior of the reset sequence (unidirectional)
    task body();
        // set reset values, randomize() cannot be used here
        default_RST     = ~RST_ACT_LEVEL;
        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item();      

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CYCLE_L;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CYCLE_H;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item();  

        default_ADDRESS = 8'h0c;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item();  

    endtask: body

endclass: timer_t_sequence_read_valid


class timer_t_sequence_read_unaligned extends timer_t_sequence;

    // registration of object tools
    `uvm_object_utils( timer_t_sequence_read_unaligned )

    // Constructor - creates new instance of this class
    function new( string name = "timer_t_sequence_read_unaligned" );
        super.new( name );
    endfunction: new

    // body - implements behavior of the reset sequence (unidirectional)
    task body();
        // set reset values, randomize() cannot be used here
        default_RST     = ~RST_ACT_LEVEL;
        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();

        default_ADDRESS = 8'h01;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item();      

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item();  

        default_ADDRESS = 8'h0f;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item();  

        default_ADDRESS = 8'h05;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item();  

    endtask: body

endclass: timer_t_sequence_read_unaligned


class timer_t_sequence_read_oor extends timer_t_sequence;

    // registration of object tools
    `uvm_object_utils( timer_t_sequence_read_oor )

    // Constructor - creates new instance of this class
    function new( string name = "timer_t_sequence_read_oor" );
        super.new( name );
    endfunction: new

    // body - implements behavior of the reset sequence (unidirectional)
    task body();
        // set reset values, randomize() cannot be used here
        default_RST     = ~RST_ACT_LEVEL;
        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();

        default_ADDRESS = 8'h15;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item();      

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item();  

        default_ADDRESS = 8'h20;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item(); 

        default_ADDRESS = 8'hff;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item();  

    endtask: body

endclass: timer_t_sequence_read_oor

class timer_t_sequence_write_valid extends timer_t_sequence;

    // registration of object tools
    `uvm_object_utils( timer_t_sequence_write_valid )

    // Constructor - creates new instance of this class
    function new( string name = "timer_t_sequence_write_valid" );
        super.new( name );
    endfunction: new

    // body - implements behavior of the reset sequence (unidirectional)
    task body();
        // set reset values, randomize() cannot be used here
        default_RST     = ~RST_ACT_LEVEL;
        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();      

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CYCLE_L;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CYCLE_H;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();  

        default_ADDRESS = 8'h0c;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();  

    endtask: body

endclass: timer_t_sequence_write_valid


class timer_t_sequence_write_unaligned extends timer_t_sequence;

    // registration of object tools
    `uvm_object_utils( timer_t_sequence_write_unaligned )

    // Constructor - creates new instance of this class
    function new( string name = "timer_t_sequence_write_unaligned" );
        super.new( name );
    endfunction: new

    // body - implements behavior of the reset sequence (unidirectional)
    task body();
        // set reset values, randomize() cannot be used here
        default_RST     = ~RST_ACT_LEVEL;
        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();

        default_ADDRESS = 8'h01;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();      

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();  

        default_ADDRESS = 8'h0f;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();  

        default_ADDRESS = 8'h05;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();  

    endtask: body

endclass: timer_t_sequence_write_unaligned


class timer_t_sequence_write_oor extends timer_t_sequence;

    // registration of object tools
    `uvm_object_utils( timer_t_sequence_write_oor )

    // Constructor - creates new instance of this class
    function new( string name = "timer_t_sequence_write_oor" );
        super.new( name );
    endfunction: new

    // body - implements behavior of the reset sequence (unidirectional)
    task body();
        // set reset values, randomize() cannot be used here
        default_RST     = ~RST_ACT_LEVEL;
        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item();

        default_ADDRESS = 8'h15;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();      

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();  

        default_ADDRESS = 8'h20;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item(); 

        default_ADDRESS = 8'hff;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();  

    endtask: body

endclass: timer_t_sequence_write_oor


class timer_t_sequence_rw_trans extends timer_t_sequence;

    // registration of object tools
    `uvm_object_utils( timer_t_sequence_rw_trans )

    // Constructor - creates new instance of this class
    function new( string name = "timer_t_sequence_rw_trans" );
        super.new( name );
    endfunction: new

    // body - implements behavior of the reset sequence (unidirectional)
    task body();
        // set reset values, randomize() cannot be used here
        default_RST     = ~RST_ACT_LEVEL;
        default_ADDRESS = 0;
        default_REQUEST = 0;
        default_DATA_IN = 0;
        create_and_finish_item(); 

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_AUTO_RESTART;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item(); 

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = MODE_CONTINOUS;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item(); 

        default_ADDRESS = TIMER_CR;
        default_DATA_IN = MODE_AUTO_RESTART;
        default_REQUEST = CP_REQ_WRITE;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CR;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item(); 

        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 69;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item(); 

        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 69;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item(); 

        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 69;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CNT;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item(); 

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 69;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item(); 

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 6969;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item(); 

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_WRITE;
        default_DATA_IN = 69;
        create_and_finish_item();  

        default_ADDRESS = TIMER_CMP;
        default_REQUEST = CP_REQ_READ;
        create_and_finish_item(); 

    endtask: body

endclass: timer_t_sequence_rw_trans
